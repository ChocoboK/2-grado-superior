﻿namespace WindowsFormsEjercicio02
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Truncar1 = new System.Windows.Forms.Button();
            this.numeros = new System.Windows.Forms.TextBox();
            this.resultado = new System.Windows.Forms.Label();
            this.Truncar2 = new System.Windows.Forms.Button();
            this.Truncar3 = new System.Windows.Forms.Button();
            this.Truncar5 = new System.Windows.Forms.Button();
            this.Truncar6 = new System.Windows.Forms.Button();
            this.Trunca4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Truncar1
            // 
            this.Truncar1.Location = new System.Drawing.Point(315, 50);
            this.Truncar1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Truncar1.Name = "Truncar1";
            this.Truncar1.Size = new System.Drawing.Size(110, 54);
            this.Truncar1.TabIndex = 0;
            this.Truncar1.Text = "Truncar1";
            this.Truncar1.UseVisualStyleBackColor = true;
            this.Truncar1.Click += new System.EventHandler(this.Truncar1_Click);
            // 
            // numeros
            // 
            this.numeros.Location = new System.Drawing.Point(93, 84);
            this.numeros.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.numeros.Name = "numeros";
            this.numeros.Size = new System.Drawing.Size(100, 32);
            this.numeros.TabIndex = 1;
            this.numeros.TextChanged += new System.EventHandler(this.numeros_TextChanged);
            // 
            // resultado
            // 
            this.resultado.AutoSize = true;
            this.resultado.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.resultado.Location = new System.Drawing.Point(94, 157);
            this.resultado.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.resultado.Name = "resultado";
            this.resultado.Size = new System.Drawing.Size(99, 24);
            this.resultado.TabIndex = 2;
            this.resultado.Text = "Resultado";
            // 
            // Truncar2
            // 
            this.Truncar2.Location = new System.Drawing.Point(315, 110);
            this.Truncar2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Truncar2.Name = "Truncar2";
            this.Truncar2.Size = new System.Drawing.Size(110, 54);
            this.Truncar2.TabIndex = 3;
            this.Truncar2.Text = "Truncar2";
            this.Truncar2.UseVisualStyleBackColor = true;
            this.Truncar2.Click += new System.EventHandler(this.Truncar2_Click);
            // 
            // Truncar3
            // 
            this.Truncar3.Location = new System.Drawing.Point(315, 170);
            this.Truncar3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Truncar3.Name = "Truncar3";
            this.Truncar3.Size = new System.Drawing.Size(110, 54);
            this.Truncar3.TabIndex = 4;
            this.Truncar3.Text = "Truncar3";
            this.Truncar3.UseVisualStyleBackColor = true;
            this.Truncar3.Click += new System.EventHandler(this.Truncar3_Click);
            // 
            // Truncar5
            // 
            this.Truncar5.Location = new System.Drawing.Point(433, 110);
            this.Truncar5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Truncar5.Name = "Truncar5";
            this.Truncar5.Size = new System.Drawing.Size(110, 54);
            this.Truncar5.TabIndex = 6;
            this.Truncar5.Text = "Truncar5";
            this.Truncar5.UseVisualStyleBackColor = true;
            this.Truncar5.Click += new System.EventHandler(this.Truncar5_Click);
            // 
            // Truncar6
            // 
            this.Truncar6.Location = new System.Drawing.Point(433, 170);
            this.Truncar6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Truncar6.Name = "Truncar6";
            this.Truncar6.Size = new System.Drawing.Size(110, 54);
            this.Truncar6.TabIndex = 7;
            this.Truncar6.Text = "Truncar6";
            this.Truncar6.UseVisualStyleBackColor = true;
            this.Truncar6.Click += new System.EventHandler(this.Truncar6_Click);
            // 
            // Trunca4
            // 
            this.Trunca4.Location = new System.Drawing.Point(433, 50);
            this.Trunca4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Trunca4.Name = "Trunca4";
            this.Trunca4.Size = new System.Drawing.Size(110, 54);
            this.Trunca4.TabIndex = 8;
            this.Trunca4.Text = "Truncar4";
            this.Trunca4.UseVisualStyleBackColor = true;
            this.Trunca4.Click += new System.EventHandler(this.Trunca4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(583, 273);
            this.Controls.Add(this.Trunca4);
            this.Controls.Add(this.Truncar6);
            this.Controls.Add(this.Truncar5);
            this.Controls.Add(this.Truncar3);
            this.Controls.Add(this.Truncar2);
            this.Controls.Add(this.resultado);
            this.Controls.Add(this.numeros);
            this.Controls.Add(this.Truncar1);
            this.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Truncar1;
        private System.Windows.Forms.TextBox numeros;
        private System.Windows.Forms.Label resultado;
        private System.Windows.Forms.Button Truncar2;
        private System.Windows.Forms.Button Truncar3;
        private System.Windows.Forms.Button Truncar5;
        private System.Windows.Forms.Button Truncar6;
        private System.Windows.Forms.Button Trunca4;
    }
}

