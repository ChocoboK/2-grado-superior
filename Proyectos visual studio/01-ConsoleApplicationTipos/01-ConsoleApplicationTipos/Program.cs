﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ConsoleApplicationTipos
{
    class Program
    {
        static void Main(string[] args)
        {
            Type tipo;

            tipo = typeof(string);
            Console.WriteLine("El nombre corto es: " + tipo.Name);
            Console.WriteLine("El nombre largo es: " + tipo.FullName);
            Console.ReadKey();

            if ((3.5 + 4) is Int32)
            {
                Console.WriteLine("Es entero");
            }
            else { 
                Console.WriteLine("No es entero");
            }
        }
    }
}
