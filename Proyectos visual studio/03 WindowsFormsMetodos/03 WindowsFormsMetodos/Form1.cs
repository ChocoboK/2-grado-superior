﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _03_WindowsFormsMetodos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void imprimirNumeros(int n1, int n2)
        {
            resultado.Text += "\n" + "Nº1:  " + n1 + "      " + "Nº2:  " + n2 + "\r\n";
        }
        private int sumarv1(int n1, int n2)
        {
            int res;
            res = n1 + n2;
            n1 = 0;
            n2 = 0;
            return res;
        }

        private int sumarv2(int n1, ref int n2)
        {
            int res;
            res = n1 + n2;
            n1 = 0;
            n2 = 0;
            return res;
        }
        private void sumarv3(int n1, int n2, out int res)
        {
            res = n1 + n2;
            n1 = 0;
            n2 = 0;
        }

        private void sumar_Click(object sender, EventArgs e)
        {
            int numero1, numero2, res;
            numero1 = Int32.Parse(texto1.Text);
            numero2 = Int32.Parse(texto2.Text);
            imprimirNumeros(numero1, numero2);

            res = sumarv1(numero1, numero2);
            resultado.Text += "La suma es: " + res;
        }

        private void sumar2_Click(object sender, EventArgs e)
        {
            int numero1, numero2, res;

            numero1 = Int32.Parse(texto1.Text);
            numero2 = Int32.Parse(texto2.Text);
            imprimirNumeros(numero1, numero2);
            res = sumarv2(numero1, ref numero2);
            imprimirNumeros(numero1, numero2);
            resultado.Text += "La suma es: " + res;
        }

        private void sumar3_Click(object sender, EventArgs e)
        {
            int numero1, numero2, res;

            numero1 = Int32.Parse(texto1.Text);
            numero2 = Int32.Parse(texto2.Text);
            imprimirNumeros(numero1, numero2);
            sumarv3(numero1, numero2, out res);
            imprimirNumeros(numero1, numero2);
            resultado.Text += "La suma es: " + res;
        }
    }
}
