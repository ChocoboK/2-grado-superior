﻿namespace _03_WindowsFormsMetodos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.texto2 = new System.Windows.Forms.TextBox();
            this.texto1 = new System.Windows.Forms.TextBox();
            this.sumar = new System.Windows.Forms.Button();
            this.resultado = new System.Windows.Forms.Label();
            this.sumar2 = new System.Windows.Forms.Button();
            this.sumar3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // texto2
            // 
            this.texto2.Location = new System.Drawing.Point(307, 146);
            this.texto2.Name = "texto2";
            this.texto2.Size = new System.Drawing.Size(89, 26);
            this.texto2.TabIndex = 0;
            // 
            // texto1
            // 
            this.texto1.Location = new System.Drawing.Point(193, 146);
            this.texto1.Name = "texto1";
            this.texto1.Size = new System.Drawing.Size(83, 26);
            this.texto1.TabIndex = 1;
            // 
            // sumar
            // 
            this.sumar.Location = new System.Drawing.Point(435, 124);
            this.sumar.Name = "sumar";
            this.sumar.Size = new System.Drawing.Size(107, 68);
            this.sumar.TabIndex = 2;
            this.sumar.Text = "Sumar v1";
            this.sumar.UseVisualStyleBackColor = true;
            this.sumar.Click += new System.EventHandler(this.sumar_Click);
            // 
            // resultado
            // 
            this.resultado.AutoSize = true;
            this.resultado.Location = new System.Drawing.Point(172, 226);
            this.resultado.Name = "resultado";
            this.resultado.Size = new System.Drawing.Size(131, 19);
            this.resultado.TabIndex = 3;
            this.resultado.Text = "Los numeros son: ";
            // 
            // sumar2
            // 
            this.sumar2.Location = new System.Drawing.Point(435, 210);
            this.sumar2.Name = "sumar2";
            this.sumar2.Size = new System.Drawing.Size(107, 68);
            this.sumar2.TabIndex = 4;
            this.sumar2.Text = "Sumar v2";
            this.sumar2.UseVisualStyleBackColor = true;
            this.sumar2.Click += new System.EventHandler(this.sumar2_Click);
            // 
            // sumar3
            // 
            this.sumar3.Location = new System.Drawing.Point(435, 297);
            this.sumar3.Name = "sumar3";
            this.sumar3.Size = new System.Drawing.Size(107, 68);
            this.sumar3.TabIndex = 5;
            this.sumar3.Text = "Sumar v3";
            this.sumar3.UseVisualStyleBackColor = true;
            this.sumar3.Click += new System.EventHandler(this.sumar3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(814, 478);
            this.Controls.Add(this.sumar3);
            this.Controls.Add(this.sumar2);
            this.Controls.Add(this.resultado);
            this.Controls.Add(this.sumar);
            this.Controls.Add(this.texto1);
            this.Controls.Add(this.texto2);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Indigo;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox texto2;
        private System.Windows.Forms.TextBox texto1;
        private System.Windows.Forms.Button sumar;
        private System.Windows.Forms.Label resultado;
        private System.Windows.Forms.Button sumar2;
        private System.Windows.Forms.Button sumar3;
    }
}

