﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02_WindowsFormsIterativas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int numero;

            resultado.Text = "";
            numero = Int32.Parse(texto.Text);
            for (int i=1; i<=numero; i++)
            {
                //resultado.Text += i;
                if (i != numero)
                {
                    resultado.Text += (i + " - ");
                } else
                {
                    resultado.Text += i;
                }
            }
        }

        private void imprimirTabla_Click(object sender, EventArgs e)
        {
            int[] tabla = { 11, 22, 33, 44, 55 };

            // for tradicional
            /*for (int i=0; i < tabla.Length; i++)
            {
                resultado.Text += tabla[i] + " ";
            }*/

            //for solo para colecciones
            foreach (int numero in tabla) 
            {
                resultado.Text += numero + " ";
            }
        }
    }
}
