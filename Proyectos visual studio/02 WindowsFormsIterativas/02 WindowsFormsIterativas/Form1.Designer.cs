﻿namespace _02_WindowsFormsIterativas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultado = new System.Windows.Forms.Label();
            this.imprimir = new System.Windows.Forms.Button();
            this.texto = new System.Windows.Forms.TextBox();
            this.fecha = new System.Windows.Forms.DateTimePicker();
            this.imprimirTabla = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // resultado
            // 
            this.resultado.AutoSize = true;
            this.resultado.Font = new System.Drawing.Font("Comic Sans MS", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultado.Location = new System.Drawing.Point(361, 188);
            this.resultado.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.resultado.Name = "resultado";
            this.resultado.Size = new System.Drawing.Size(97, 23);
            this.resultado.TabIndex = 0;
            this.resultado.Text = "Resultado: ";
            this.resultado.Click += new System.EventHandler(this.label1_Click);
            // 
            // imprimir
            // 
            this.imprimir.Location = new System.Drawing.Point(490, 264);
            this.imprimir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.imprimir.Name = "imprimir";
            this.imprimir.Size = new System.Drawing.Size(161, 36);
            this.imprimir.TabIndex = 1;
            this.imprimir.Text = "Imprimir numeros";
            this.imprimir.UseVisualStyleBackColor = true;
            this.imprimir.Click += new System.EventHandler(this.button1_Click);
            // 
            // texto
            // 
            this.texto.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.texto.Location = new System.Drawing.Point(433, 226);
            this.texto.Name = "texto";
            this.texto.Size = new System.Drawing.Size(281, 30);
            this.texto.TabIndex = 3;
            // 
            // fecha
            // 
            this.fecha.Location = new System.Drawing.Point(890, 12);
            this.fecha.Name = "fecha";
            this.fecha.Size = new System.Drawing.Size(334, 30);
            this.fecha.TabIndex = 6;
            // 
            // imprimirTabla
            // 
            this.imprimirTabla.Location = new System.Drawing.Point(490, 310);
            this.imprimirTabla.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.imprimirTabla.Name = "imprimirTabla";
            this.imprimirTabla.Size = new System.Drawing.Size(161, 36);
            this.imprimirTabla.TabIndex = 7;
            this.imprimirTabla.Text = "Imprimir Tabla";
            this.imprimirTabla.UseVisualStyleBackColor = true;
            this.imprimirTabla.Click += new System.EventHandler(this.imprimirTabla_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(1236, 631);
            this.Controls.Add(this.imprimirTabla);
            this.Controls.Add(this.fecha);
            this.Controls.Add(this.texto);
            this.Controls.Add(this.imprimir);
            this.Controls.Add(this.resultado);
            this.Font = new System.Drawing.Font("Comic Sans MS", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Indigo;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Sentencias iterativas";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label resultado;
        private System.Windows.Forms.Button imprimir;
        private System.Windows.Forms.TextBox texto;
        private System.Windows.Forms.DateTimePicker fecha;
        private System.Windows.Forms.Button imprimirTabla;
    }
}

