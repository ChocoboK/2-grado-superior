// ConsoleApplicationFicheroPersonas.cpp: define el punto de entrada de la aplicación de consola.

/*

Gestionar datos alumnos
- Nombre
- Edad
MAXIMO 10 ALUMNOS

1. Cargar datos del fichero
2. Escribir datos en el fichero
3. Insertar en tabla
4. Mostrar tabla en pantalla
5. Borrar tabla(Huecos)
6. Salir

*/

#include "stdafx.h"
#include <stdio.h>
#include <string.h>

// Declarando la estructura de alumnos //
struct TAlumno
{
	char nombre[40];
	int edad;
};

// Funcion para el menu //
void imprimirMenu() {
	printf("   ==> MENU DE OPCIONES <==   \n");
	printf("------------------------------\n");
	printf("1. Cargar datos del fichero	  \n");
	printf("2. Escribir datos del fichero \n");
	printf("3. Insertar en la tabla		  \n");
	printf("4. Mostrar tabla en pantalla  \n");
	printf("5. Borrar tabla 			  \n");
	printf("6. Salir 					  \n");

	printf("Eliga una de las opciones: ");
}

// Funcion para cargar los datos //
void cargarDatos(char nombreFichero[50], struct TAlumno tablaAlumnos[10], int *numero)
{
	FILE *pf;
	char nombreAlumno[40];
	int edadAlumno,
		posicion = 0;
		*numero = 0;

	pf = fopen(nombreFichero, "rt");
	if (pf != NULL)
	{

		while (!feof(pf)) // feof (File end of file) sirve para que lea hasta que llegue al final del fichero.
		{
			fscanf(pf, "%s", nombreAlumno);
			fscanf(pf, "%i", &edadAlumno);
			strcpy(tablaAlumnos[posicion].nombre, nombreAlumno); // se usa strcpy porque (tablaAlumnos[posicion].nombre, nombreAlumno) no es una variable inmediata.
			tablaAlumnos[posicion].edad = edadAlumno;
			posicion++;
		}
		(*numero) = posicion;
		fclose(pf);
	}
}

// Funcion para escribir los datos //
void escribirDatos(char nombreFichero[50], struct TAlumno tablaAlumnos[10], int numero) {
	FILE *pf;
	int posicion = 0;

	pf = fopen(nombreFichero, "wt");
	if (pf != NULL)
	{
		for (posicion=0; posicion < numero; posicion++)
		{
			fprintf(pf, "%s\n", tablaAlumnos[posicion].nombre);
			fprintf(pf, "%i\n", tablaAlumnos[posicion].edad);
		}
		fclose(pf);
	}
}

// Funcion para mostrar tabla en pantalla //
void muestraTabla(char nombreFichero[50], struct TAlumno tablaAlumnos[10], int numero) {

	int posicion;

	for (posicion = 0; posicion < numero; posicion++)
	{
		printf("\t%s\n", tablaAlumnos[posicion].nombre);
		printf("\t%i\n", tablaAlumnos[posicion].edad);
	}
	printf("\n");
}

// Funcion para insertar tabla //
void insertarTabla(char nombreFichero[50], struct TAlumno tablaAlumnos[10], int numero) {
	
	char nombreAlumno[40];
	int edadAlumno,
	    posicion;

	printf("Dime un nombre: ");
	scanf("%s", nombreAlumno);
	printf("Dime la edad: ");
	scanf("%i", &edadAlumno);

	for (posicion = 0; posicion < numero; posicion++)
	{
		printf("\t%s\n", tablaAlumnos[posicion].nombre);
		printf("\t%i\n", tablaAlumnos[posicion].edad);
	}

	printf("\n");

}


// Funcion para borrar la tabla //
void borraTabla() {

}

// Programa principal //
int main()
{
	int opcion = 0;
	struct TAlumno tablaAlumnos[10];
	int numeroAlumnos;

	numeroAlumnos = 0;
	do
	{
		imprimirMenu();
		scanf("%i", &opcion);
		printf("\n");
		switch (opcion)
		{
		case 1: cargarDatos("clase.txt", tablaAlumnos, &numeroAlumnos);
			break;
	
		case 2: escribirDatos("clase2.txt", tablaAlumnos, numeroAlumnos);
			break;

		case 3: muestraTabla("clase.txt", tablaAlumnos, numeroAlumnos);
			break;

		case 4: insertarTabla("clase.txt", tablaAlumnos, numeroAlumnos);
			break;
	/*
		case 5: borraTabla();
			break;

		case 6:
			printf("Hasta luego!\n");
			return 0;
			break;
    */
		default: printf("Te has colado :) \n");
		}
	} while (opcion != 6);

	return 0;
}