﻿namespace WindowsFormsEjercicio01
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.mostrar = new System.Windows.Forms.Button();
            this.resultado = new System.Windows.Forms.Label();
            this.texto = new System.Windows.Forms.TextBox();
            this.rotarD = new System.Windows.Forms.Button();
            this.mayusculas = new System.Windows.Forms.Button();
            this.invertir = new System.Windows.Forms.Button();
            this.rotarI = new System.Windows.Forms.Button();
            this.minusculas = new System.Windows.Forms.Button();
            this.salir = new System.Windows.Forms.Button();
            this.vaciar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // mostrar
            // 
            this.mostrar.Location = new System.Drawing.Point(510, 35);
            this.mostrar.Name = "mostrar";
            this.mostrar.Size = new System.Drawing.Size(121, 84);
            this.mostrar.TabIndex = 0;
            this.mostrar.Text = "Mostrar por pantalla";
            this.mostrar.UseVisualStyleBackColor = true;
            this.mostrar.Click += new System.EventHandler(this.mostrar_Click);
            // 
            // resultado
            // 
            this.resultado.AutoSize = true;
            this.resultado.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.resultado.Location = new System.Drawing.Point(137, 133);
            this.resultado.Name = "resultado";
            this.resultado.Size = new System.Drawing.Size(85, 21);
            this.resultado.TabIndex = 1;
            this.resultado.Text = "Resultado";
            this.resultado.Click += new System.EventHandler(this.resultado_Click);
            // 
            // texto
            // 
            this.texto.Location = new System.Drawing.Point(130, 64);
            this.texto.Name = "texto";
            this.texto.Size = new System.Drawing.Size(332, 29);
            this.texto.TabIndex = 2;
            // 
            // rotarD
            // 
            this.rotarD.Location = new System.Drawing.Point(510, 125);
            this.rotarD.Name = "rotarD";
            this.rotarD.Size = new System.Drawing.Size(121, 84);
            this.rotarD.TabIndex = 3;
            this.rotarD.Text = "Rotar a derecha";
            this.rotarD.UseVisualStyleBackColor = true;
            this.rotarD.Click += new System.EventHandler(this.rotarD_Click);
            // 
            // mayusculas
            // 
            this.mayusculas.Location = new System.Drawing.Point(510, 215);
            this.mayusculas.Name = "mayusculas";
            this.mayusculas.Size = new System.Drawing.Size(121, 84);
            this.mayusculas.TabIndex = 4;
            this.mayusculas.Text = "Convertir a Mayusculas";
            this.mayusculas.UseVisualStyleBackColor = true;
            this.mayusculas.Click += new System.EventHandler(this.mayusculas_Click);
            // 
            // invertir
            // 
            this.invertir.Location = new System.Drawing.Point(637, 35);
            this.invertir.Name = "invertir";
            this.invertir.Size = new System.Drawing.Size(121, 84);
            this.invertir.TabIndex = 5;
            this.invertir.Text = "Invertir cadena";
            this.invertir.UseVisualStyleBackColor = true;
            this.invertir.Click += new System.EventHandler(this.invertir_Click);
            // 
            // rotarI
            // 
            this.rotarI.Location = new System.Drawing.Point(637, 125);
            this.rotarI.Name = "rotarI";
            this.rotarI.Size = new System.Drawing.Size(121, 84);
            this.rotarI.TabIndex = 6;
            this.rotarI.Text = "Rotar a izquierda";
            this.rotarI.UseVisualStyleBackColor = true;
            this.rotarI.Click += new System.EventHandler(this.rotarI_Click);
            // 
            // minusculas
            // 
            this.minusculas.Location = new System.Drawing.Point(637, 215);
            this.minusculas.Name = "minusculas";
            this.minusculas.Size = new System.Drawing.Size(121, 84);
            this.minusculas.TabIndex = 7;
            this.minusculas.Text = "Convertir a Minusculas";
            this.minusculas.UseVisualStyleBackColor = true;
            this.minusculas.Click += new System.EventHandler(this.minusculas_Click);
            // 
            // salir
            // 
            this.salir.Location = new System.Drawing.Point(793, 12);
            this.salir.Name = "salir";
            this.salir.Size = new System.Drawing.Size(78, 51);
            this.salir.TabIndex = 8;
            this.salir.Text = "Salir";
            this.salir.UseVisualStyleBackColor = true;
            this.salir.Click += new System.EventHandler(this.salir_Click);
            // 
            // vaciar
            // 
            this.vaciar.Location = new System.Drawing.Point(28, 52);
            this.vaciar.Name = "vaciar";
            this.vaciar.Size = new System.Drawing.Size(78, 51);
            this.vaciar.TabIndex = 9;
            this.vaciar.Text = "Vaciar";
            this.vaciar.UseVisualStyleBackColor = true;
            this.vaciar.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(883, 336);
            this.Controls.Add(this.vaciar);
            this.Controls.Add(this.salir);
            this.Controls.Add(this.minusculas);
            this.Controls.Add(this.rotarI);
            this.Controls.Add(this.invertir);
            this.Controls.Add(this.mayusculas);
            this.Controls.Add(this.rotarD);
            this.Controls.Add(this.texto);
            this.Controls.Add(this.resultado);
            this.Controls.Add(this.mostrar);
            this.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button mostrar;
        private System.Windows.Forms.Label resultado;
        private System.Windows.Forms.TextBox texto;
        private System.Windows.Forms.Button rotarD;
        private System.Windows.Forms.Button mayusculas;
        private System.Windows.Forms.Button invertir;
        private System.Windows.Forms.Button rotarI;
        private System.Windows.Forms.Button minusculas;
        private System.Windows.Forms.Button salir;
        private System.Windows.Forms.Button vaciar;
    }
}

