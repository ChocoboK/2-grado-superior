#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

int bandera_hilo;
pthread_cond_t bandera_hilo_cv; // cv = condition variable
pthread_mutex_t bandera_hilo_mutex;

// Inicializamos todas las variables
void inicializar_bandera() {
    pthread_mutex_init (&bandera_hilo_mutex, NULL);
    pthread_cond_init (&bandera_hilo_cv, NULL);
    bandera_hilo = 0;
}
	
// pondremos el valor de "bandera_hilo" en "bandera_valor"
void poner_bandera_hilo (int bandera_valor) { // aqui vamos poner un lock(mutex) y unlock(mutex) para que la señal no se pierda, igual que arriba rodeamos con un lock y unlock
    // bloquea el mutex antes de acceder al valor
    pthread_mutex_lock (&bandera_hilo_mutex); // ponemos el valor de la bandera y entonces la señal en "funcion_hilo" es bloqueada, esperando a la "bandera" tenga valor. Sin embargo, "funcion_hilo"
    bandera_hilo = bandera_valor;             // no puede comprobar la bandera hasta que mutex este desbloqueado.
    pthread_cond_signal (&bandera_hilo_cv);
    pthread_mutex_unlock (&bandera_hilo_mutex); // se desbloquea mutex
}

int i;
void do_work() {	
    while(i <= 50) {
	i++;
	poner_bandera_hilo(1);
	//printf("\rRepeticion numero: %i",i);
	//fflush(stdout);
	//i++;
	sleep(1);
    }
}

void *do_work2(void *argumentos_hilo) {
    while(i <=50) {
        if (i%5==0) {
	   printf("\rAhora vale: %i", i);
	   fflush(stdout);
	   poner_bandera_hilo(0);
	   sleep(1);
	}
    }
}

void *funcion_hilo (void *argumentos_hilo) {
    while (1) {
	pthread_mutex_lock (&bandera_hilo_mutex); // bloqueamos la variable mutex antes de acceder al valor
	while (!bandera_hilo) {					      // la "bandera" esta vacia, esperando por la señal de la condicion variable, indicando que el valor de la "bandera" ha cambiado.
	    pthread_cond_wait(&bandera_hilo_cv, &bandera_hilo_mutex); // cuando la señal llegue y este hilo se desbloquee, se hara el bucle y comprobara la "bandera" de nuevo.
	}
	pthread_mutex_unlock (&bandera_hilo_mutex);		      // cuando se llegue aqui, sabemos que la "bandera" debe tener un valor. Desbloqueamos el mutex.
	do_work(); // para que trabaje
    }
    return NULL;
}

//Funcion punto de entrada
int main(){
    pthread_t cuenta, valorActual;
    inicializar_bandera();
    poner_bandera_hilo(1);

    pthread_create(&cuenta, NULL, &funcion_hilo, NULL);
    pthread_create(&valorActual, NULL, &do_work2, NULL);
    pthread_join(cuenta, NULL);
    pthread_join(valorActual, NULL);

    printf("FIN\n");

    return EXIT_SUCCESS;
}

