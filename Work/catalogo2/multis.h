#ifndef __MULTIS_H
#define __MULTIS_H

#ifdef __cplusplus
extern "C" {
#endif

    const char **catalogo();
    int mul (int op1, int op2);
    int div (int op1, int op2);

#ifdef __cplusplus
}
#endif



#endif
