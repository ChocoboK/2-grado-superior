#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <unistd.h>

typedef int temp_file_handle; // Transforma temp_file_handle en un entero (int)

temp_file_handle write_temp_file (char* buffer, size_t length) { 
	char temp_filename[] = "/tmp/temp_file.XXXXXX"; // XXXXXX sera remplazado por los caracteres del nombre del fichero
	int fd = mkstemp (temp_filename);
	unlink (temp_filename);
	write (fd, &length, sizeof (length));
	write (fd, buffer, length);
	return fd;
}

char* read_temp_file (temp_file_handle temp_file, size_t* length) {
	char * buffer;
	int fd = temp_file;
	lseek (fd, 0, SEEK_SET); // Volvera al principio del fichero
	read (fd, length, sizeof (*length));
	buffer = (char*) malloc (*length);
	read (fd, buffer, *length);
	close (fd);
	return buffer;
}

int main(void) {
	int temp_file, resultado, resultado2;
	size_t length = sizeof(length);
	char buf[] = "Hola";

	resultado = write_temp_file(buf, length);
	resultado2 = read_temp_file(temp_file, &length);

	printf("%i\n", resultado2);

	return EXIT_SUCCESS;
}
