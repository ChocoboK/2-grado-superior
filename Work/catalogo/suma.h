#ifndef __SUMA_H__
#define __SUMA_H__

#ifdef __cplusplus
extern "C" {
#endif

    int sumar  (int n1, int n2);
    
#ifdef __cplusplus
}
#endif

#endif
