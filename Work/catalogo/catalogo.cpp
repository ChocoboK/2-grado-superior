#include <stdio.h>
#include <stdlib.h>

#include "suma.h"
#include "resta.h"

void catalogo(){
	int opcion, n1, n2, resultado;
	printf("Quieres sumar(1) o restar(2)\n");
	scanf("%i",&opcion);

	switch (opcion) {
		case 1:
			printf("Numeros a sumar (n1,n2) : ");
			scanf("%i,%i", &n1, &n2);
			resultado = sumar(n1,n2);
			printf("Resultado de la suma= %i\n", resultado);
			break;
		case 2:
			printf("Numeros a restar (n1,n2): ");
			scanf("%i,%i", &n1, &n2);
			resultado = restar(n1,n2);
			printf("Resultado de la resta= %i\n", resultado);
			break;
		default:
			printf("Introduce 1 (sumar) o 2 (restar)\n");
			break;
	}
}
