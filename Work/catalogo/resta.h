#ifndef __RESTAR_H__
#define __RESTAR_H__

#ifdef __cplusplus
extern "C" {
#endif

    int restar (int n1, int n2);
    
#ifdef __cplusplus
}
#endif

#endif
