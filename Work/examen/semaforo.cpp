#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
sem_t sem_id;

int Lllevar = 0;
int LTotales = 0;
int LFabrica = 0;
int LAlmacenados = 20;

void llevar() {
    int numero = 0;
    numero = rand();

    if(numero%2 == 0){
	LTotales=1;
    }else if (LFabrica == 20 -1) {
	LTotales = 1;
    } else {
	LTotales = 2;
    }
}

void *UnoTrabajar(void *args) {
    while (Lllevar < 20) {
	sem_wait(&sem_id);
	pthread_mutex_lock(&mutex);

	while(LFabrica < 20) {
	    llevar();
	    printf("Trabajador1 lleva %i ladrillos \n", LTotales);
	    LFabrica += LTotales;
	    Lllevar += LTotales;
	    printf("En la fabrica ahora hay %i ladrillos \n", LFabrica);
	}
	pthread_mutex_unlock(&mutex);
	sem_post(&sem_id);

	if(LFabrica < 20){
	    printf("Trabajador1 deja a Trabajador2 \n");
	} else {
	    printf("Trabajador1 ha llevado el ultimo ladrillo y duerme\n");
	    sleep(2);
	}
	return NULL;
    }
}

void *DosTrabajar(void *args) {
    while (Lllevar < 20) {
	sem_wait(&sem_id);
	pthread_mutex_lock(&mutex);

	while(LFabrica < 20) {
	    llevar();
	    printf("Trabajador2 lleva %i ladrillos \n", LTotales);
	    LFabrica += LTotales;
	    Lllevar += LTotales;
	    printf("En la fabrica ahora hay %i ladrillos \n", LFabrica);
	}
	pthread_mutex_unlock(&mutex);
	sem_post(&sem_id);

	if(LFabrica < 20){
	    printf("Trabajador2 deja a Trabajador1 \n");
	} else {
	    printf("Trabajador2 ha llevado el ultimo ladrillo y duerme\n");
	    sleep(2);
	}
	return NULL;
    }
}


//Funcion punto de entrada
int main(int argc, char *argv[]){
    pthread_t trabajador1;
    pthread_t trabajador2;

    srand(time(NULL));
    sem_init(&sem_id, 0, 1);
    pthread_create(&trabajador1, NULL, &UnoTrabajar, &sem_id);
    pthread_create(&trabajador2, NULL, &DosTrabajar, &sem_id);
    
    pthread_join (trabajador1, NULL);
    printf("Trabajador 1 ha acabado su tarea\n");
    pthread_join (trabajador2, NULL);
    printf("Trabajador 2 ha acabado su tarea\n");

    sem_destroy(&sem_id);

    return EXIT_SUCCESS;
}
