#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

int spawn(char* program, char** arg_list) {
    pid_t child_pid;	// pid_t es alias de int

    child_pid = fork();	// fork divide el proceso en 2, siendo identico al padre

    // se comprobara si al padre le ha devuelto un valor
    if (child_pid != 0) { 
      return child_pid;	// si devuelve otro valor como 0, se ejecuta el hijo
    } else {
      // ejecutara el programa hijo
      execvp(program, arg_list);
      fprintf(stderr, "Error al ejecutar el programa\n");
      abort();
    }
}

int main() {
    // Lista de parametros
    int child_status;
    int opcion;

    char* arg_list2[] = {
	"./resta",
	"4",
	"3",
	NULL
    };

    char* arg_list[] = {
        "./suma",
	"4",
        "3",
	NULL
    };

    printf("Quiere sumar(1) o restar(2) \n");
    scanf("%i", &opcion);

    if (opcion == 1){
    spawn("./suma", arg_list);
    } else if (opcion == 2) {
    spawn("./resta", arg_list);
    }
    wait(&child_status);
      if(WIFEXITED (child_status)){
	  printf(" El procceso hijo ha salido bien, tiene el codigo: %d\n", WIFEXITED (child_status));
	  printf(" La suma es : %d\n", WEXITSTATUS (child_status));
      } else {
          printf (" El proceso hijo no ha salido bien\n");
      }
    printf("El programa ha acabado la ejecucion.\n");

    return 0;
}

