#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//Funcion punto de entrada
int main(int argc, char *argv[]){

	int n1, n2, resultado;

	n1 = atoi(argv[1]);
	n2 = atoi(argv[2]);

	resultado = n1 - n2;
	
	return resultado;
}

