#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int spawn(char* program, char** arg_list) {
    pid_t child_pid;	// pid_t es alias de int

    child_pid = fork();	// fork divide el proceso en 2, identico al padre

    // se comprobara si al padre le ha devuelto un valor
    if (child_pid != 0) { 
      return child_pid;	// si devuelve otro valor como 0, se ejecuta el hijo
    } else {
      // ejecutara el programa hijo
      execvp(program, arg_list);
      fprintf(stderr, "Error al ejecutar el programa\n");
      abort();
    }
}

int main() {
    // Lista de parametros
    char* arg_list[] = {
        "ls",
        "-la",
        "/",
        NULL
    };
    spawn("ls", arg_list);
    printf("El programa ha acabado la ejecucion.\n");

    return 0;
}

