
#include <stdio.h>
#include <stdlib.h>

#include "aritmetica.h"

//Funcion punto de entrada
int main(int argc, char *argv[]){

	int n1, n2;	
	
	printf("Dime dos numeros para sumar y restar (x,y) \n");
	scanf("%i,%i", &n1, &n2);

	printf("Suma: %i \nResta: %i \n", sumar(n1,n2), restar(n1,n2));
	
	return EXIT_SUCCESS;
}

