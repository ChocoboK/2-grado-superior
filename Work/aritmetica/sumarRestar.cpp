#include <stdio.h>
#include <stdlib.h>

#include "aritmetica.h"

int sumar (int n1, int n2){
	int res;
	res = n1 + n2;
	return res;
}

int restar (int n1, int n2){
	int res;
	res = n1 - n2;
	return res;
}
