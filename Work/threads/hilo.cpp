#include <pthread.h>
#include <stdio.h>

// Parametros para imprimir la funcion

struct char_print_parms {
// Caracter a imprimir
  char character;
// Numero de veces a imprimir
  int count;
};

// Pinta un numero de caracteres por el stderr, dado por los PARAMETROS, el cual es un puntero a la estructura de arriba
void* char_print (void* parameters) {
  // Se hace un molde al puntero del tipo correcto
  struct char_print_parms* p = (struct char_print_parms*) parameters;
  int i;

  for (i = 0; i < p->count; ++i) {
    fputc (p->character, stderr);
  }
  return NULL;
}

// Main

int main() {
pthread_t thread1_id;
pthread_t thread2_id;
struct char_print_parms thread1_args;
struct char_print_parms thread2_args;

// Crea un nuevo hilo para imprimir
thread1_args.character = 'x';
thread1_args.count = 30000;
pthread_create (&thread1_id, NULL, &char_print, &thread1_args);

// Crea un nuevo hilo para imprimir
thread2_args.character = 'o';
thread2_args.count = 20000;
pthread_create (&thread2_id, NULL, &char_print, &thread2_args);

// Ahora pthread_join no cierra el hilo hasta que acabe
pthread_join (thread1_id, NULL);
pthread_join (thread2_id, NULL);

return 0;

}

