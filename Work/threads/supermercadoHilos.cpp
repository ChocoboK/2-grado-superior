#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <malloc.h>
#include <semaphore.h>

#define filename "hilo.txt"
#define N 5
#define MAX 0x100

struct parametros_caracter_imprimir {
    char character;
    int count;
};

void* imprime_caracter (void* parametros) {
    struct parametros_caracter_imprimir* p = (struct parametros_caracter_imprimir*) parametros;
    int i;

    for(i = 0; i < p->count; ++i) {
	fprintf (stderr, "\r%c\n", p->character);
	sleep(2);
    }
    fprintf(stderr, "%lu\n", pthread_self());
    return NULL;
}

void initialize_semaphore() {
    thread_args = NULL;
    sem_init (thread_args_count, 0, 0);
}

//Funcion punto de entrada
int main(int argc, char *argv[]){

    int opcion;
    char caracter;
    int numero;
    int id;
    FILE *pf;
    char nombre[N][MAX];
    int idHilo;

    system("rm " filename);

    if (! (pf = fopen (filename, "a")) )
	fprintf (stderr, "Couldn't find your %s\n", nombre[MAX]); 
    struct parametros_caracter_imprimir thread_args[5];
    pthread_t thread_id [5] = {0, 0, 0, 0, 0};

    pthread_mutex_t thread_args_mutex = PTHREAD_MUTEX_INITIALIZER; 
    
    do {
	system("clear");
	printf("B I E N V E N I D O\n");
	printf("1. Crear hilo	   \n");
	printf("2. Cepillar hilo   \n");
	printf("3. Salir 	   \n");
	printf("Dime una opcion: \n");
	scanf("%i", &opcion);

	switch(opcion) {
	    case 1:
		printf(" \nCrear hilo\n Dime id del hilo (hay 0-4 (5)hilos), un caracter (ej. x) y un numero de veces (ej. 10)\n");
		scanf("%i %c %i", &id, &caracter, &numero);


		if ( id < 4) {
		    thread_args[id].character = caracter;
		    thread_args[id].count = numero;

		    pthread_create (&thread_id[id], NULL, &imprime_caracter, &thread_args[id]);

		    fprintf (pf, "%lu\n", thread_id[id]);
		}
		fflush(pf);
		break;

	    case 2:
		printf(" \nEliminar hilo\n Dime que hilo me tengo que cepillar\n");
		scanf("%i", &id);
		pthread_cancel(thread_id[id]);		
		
		break;

	    case 3:
		// Ahora hay que hacer un "cajero", usando un semaforo o usando mutex
		while(1) {
			struct parametros_caracter_imprimir* next_thread_args;
			sem_wait(&thread_args_count);
			pthread_mutex_lock(&thread_args_mutex);
			next_thread_args = thread_args;
			thread_args = thread_args->next;
			pthread_mutex_unlock(&thread_args_mutex);
			procces_job(next_thread_args);
			free(next_thread_args);
		}

		/*
		printf("Hilos: \n");
		for (int i = 0; i < 3; i++) {
		    if (thread_id[i] != 0) {
			pthread_join (thread_id[i], NULL);
		    }
		}
		printf("\n");
		*/
		break;

	    default:
		printf("Intenta con otro numero\n");
		break;
	} 
    } while (opcion !=3);

    fclose(pf);

    return EXIT_SUCCESS;
}

